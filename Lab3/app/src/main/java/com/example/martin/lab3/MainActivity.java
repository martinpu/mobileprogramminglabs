package com.example.martin.lab3;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends Activity implements SensorEventListener {
    private View rootView;
    private ImageView ball;

    private int backdropX;
    private int backdropY;

    private SensorManager sManager;
    private Sensor sGravity;
    private Vibrator vibrator;
    private ToneGenerator toneGenerator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.rootView = findViewById(android.R.id.content);
        this.ball = findViewById(R.id.circleShape);

        this.sManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        this.sGravity = sManager.getDefaultSensor(Sensor.TYPE_GRAVITY);

        this.vibrator = (Vibrator) this.getSystemService(VIBRATOR_SERVICE);
        this.toneGenerator = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
    }

    private void collision() {
        this.vibrator.vibrate(100);
        this.toneGenerator.startTone(ToneGenerator.TONE_DTMF_0, 100);
    }

    @Override
    protected void onResume() {
        super.onResume();

        this.rootView.getViewTreeObserver().addOnGlobalLayoutListener(() -> {

            this.backdropX = this.rootView.getWidth() - 160;
            this.backdropY = this.rootView.getHeight() - 160;

            this.ball.setX((float) backdropX / 2);
            this.ball.setY((float) backdropY / 2);

            this.sManager.registerListener(this, this.sGravity, SensorManager.SENSOR_DELAY_FASTEST);
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        this.sManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        // Get ball position
        float ballX = this.ball.getX();
        float ballY = this.ball.getY();

        float x = event.values[0];
        float y = event.values[1];

        if (ballX < 0) {
            this.ball.setX(ballX+20);
            collision();
            return;
        }
        if (ballX > this.backdropX) {
            this.ball.setX(ballX-20);
            collision();
            return;
        }
        if (ballY < 0) {
            this.ball.setY(ballY+20);
            collision();
            return;
        }
        if (ballY > this.backdropY) {
            this.ball.setY(ballY-20);
            collision();
            return;
        }

        if (x > 0.5 || x < -0.5) {
            this.ball.setY(ballY + (int)x);
        }
        if (y > 0.5 || y < -0.5) {
            this.ball.setX(ballX + (int)y);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }
}


