package com.example.martin.lab1;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

public class A2 extends AppCompatActivity {

    static final int RESULT = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set new layout.
        setContentView(R.layout.activity_a2);

        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();

        String name = intent.getStringExtra(A1.NAME);
        int count = intent.getIntExtra(A1.COUNT, 0);
        String difficulty = intent.getStringExtra(A1.DIFFICULTY);

        // Capture the layout's TextView and set the string as its text
        TextView textView = findViewById(R.id.T2);
        textView.setText("Hello " + name);
    }

    /**
     * Start the next activity.
     */
    public void getResult(View view) {
        Intent getString = new Intent(this, A3.class);
        startActivityForResult(getString, RESULT);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == RESULT) {
            // Make sure the request was successful

            if (resultCode == RESULT_OK) {

                String input = data.getStringExtra("result");

                TextView textView = findViewById(R.id.T3);
                textView.setText("From A3: " + input);

            }
        }
    }

}
