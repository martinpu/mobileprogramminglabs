package com.example.martin.lab1;;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.ArrayAdapter;


public class A1 extends AppCompatActivity {

    /**
     * Info to send to the next activity.
     */
    public static final String NAME = "com.example.lab1.NAME";
    public static final String COUNT = "com.example.lab1.COUNT";
    public static final String DIFFICULTY = "com.example.lab1.DIFFICULTY";
    public static final String STOREDDIFF = "storeddifficulty";


    /**
     * Starts the first activity.
     *
     * @param savedInstanceState Automated.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a1);

        getPreference();
        fillDropBox();
    }


    /**
     * Provides content for the drop box.
     */
    private void fillDropBox() {
        // Get spinner from XML.
        Spinner dropdown = findViewById(R.id.L1);

        // Create drop box content.
        String[] items = new String[]{"Easy", "Normal", "Hard"};

        // Create an adapter to describe how the items are displayed.
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);

        // Set the spinners adapter to the previously created one.
        dropdown.setAdapter(adapter);
    }


    /**
     * Retrieves value saved in shared preferences.
     */
    private void getPreference() {
        SharedPreferences prefs = getSharedPreferences(STOREDDIFF, MODE_PRIVATE);
        String restoredText = prefs.getString("text", null);

        if (restoredText != null) {
            int savedDiff = prefs.getInt("difficulty", 0); //0 is the default value.

            Spinner dropbox = findViewById(R.id.L1);
            dropbox.setSelection(savedDiff);
        }
    }


    /**
     * Start the next activity.
     *
     * @param view The next activity.
     */
    public void next(View view) {
        // Choose class to receive info.
        Intent intent = new Intent(this, A2.class);

        // Get chosen name and put in intent.
        EditText editText = findViewById(R.id.T1);
        String name = editText.getText().toString();
        intent.putExtra(NAME, name);

        // Get chosen SeekBar value and put in intent.
        SeekBar slider = findViewById(R.id.S1);
        int count = slider.getProgress();
        intent.putExtra(COUNT, count);

        // Get chosen value from drop box.
        Spinner dropbox = findViewById(R.id.L1);
        int difficulty = dropbox.getSelectedItemPosition();
        intent.putExtra(DIFFICULTY, difficulty);
        storeDifficulty(difficulty);

        // Start the next activity.
        startActivity(intent);
    }


    /**
     * Stores set difficulty in shared preferences.
     *
     * @param diff Selected difficulty.
     */
    private void storeDifficulty(int diff) {
        SharedPreferences.Editor editor = getSharedPreferences(STOREDDIFF, MODE_PRIVATE).edit();
        editor.putInt("difficulty", diff);
        editor.apply();
    }
}
