package com.example.martin.lab1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;


public class A3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set new layout.
        setContentView(R.layout.activity_a3);
    }


    public void goBack(View view){
        Intent returnIntent = new Intent();
        TextView textView = findViewById(R.id.T4);
        String input = textView.getText().toString();

        returnIntent.putExtra("result", input);
        setResult(A2.RESULT_OK, returnIntent);
        finish();
    }

}

